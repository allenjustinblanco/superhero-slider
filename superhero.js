// Select a topic, and select your top 5 favorites.
// Ex. Top 5 movies, cars, actors, game systems, etc.
// Convert this top favorites into an image gallery strictly using jQuery and JavaScript.
// Each selection should have a image, name of the subject, and a description.
// User must be able to a button to view the next/previous display.
// If you like, do a Top 10 favorites.
// *NOTE: You may use Bootstrap for your UI design, but NOT for your slide's functionality (The feature to view the next/previous display must be done using jQuery/JavaScript).

// get the previous button element from html
// then add a click event to that element
$('#previous').click(function() {
    // logged previous to see if the click event is happening
    console.log('previous');

    // get the current element
    let currentElement = $('.visible')
    // get the previous
    let previousElement = currentElement.prev()

    //switch classes for visibility
    currentElement.removeClass('visible')
    currentElement.addClass('visually-hidden')
    previousElement.removeClass('visually-hidden')
    previousElement.addClass('visible')

    // if the previous element is not defined
    if (previousElement.html() == undefined) {
        // log that it is not defined
        console.log("undefined prev element")

        // get the last element and display it
        let lastElem = $( "ul li" ).last()
        lastElem.removeClass('visually-hidden')
        lastElem.addClass('visible')
    }
});

$('#next').click(function() {
    console.log('next');
    let currentElement = $('.visible')
    let nextElement = currentElement.next()

    currentElement.removeClass('visible')
    currentElement.addClass('visually-hidden')
    nextElement.removeClass('visually-hidden')
    nextElement.addClass('visible')

    if (nextElement.html() == undefined) {
        console.log("here")
        let firstElem = $( "ul li" ).first()
        firstElem.removeClass('visually-hidden')
        firstElem.addClass('visible')
    }

});
